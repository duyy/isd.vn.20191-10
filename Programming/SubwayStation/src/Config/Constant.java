package Config;

public class Constant {

    //type of ticket and prepaid card
    public static final int typeOnewayTicket = 1;
    public static final int type24hTicket = 2;
    public static final int typePrepaidCard =3;

    // constant number for station to calculate fare/ modify the fare calculated formula
    public static final float baseFare = 0; // min fare
    public static final float minimumPrepaidCardBalance = 0;  // so tien toi thieu trong the
    public static final float defaultDepositPrepaidCard = 16; //tien de mua the
    public static final float ticket24hBaseFare = 0;  // tien de mua ve 24h

    //status of ticket/prepaidcard
    public static final int statusUnsed = 1;  //new
    public static final int statusNotExist = 2; //not existed in database
    public static final int statusExpired = 3;  // used once for one-way ticket/ expired for 24h ticket / lower than base fair for prepaid card
    public static final int statusUsing = 4;

    /*  pseudobarcode and ticketcode generated from ticket regconizer
        abcdefgh e8dc4081b13434b4
        bbcdefgh 2c01b64843eab357
        cbcdefgh b6dd0943e0fc90f3
        dbcdefgh 170f7c10f68a01e1
    */
}

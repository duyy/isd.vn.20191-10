DROP TABLE IF EXISTS pseudobarcode;
CREATE TABLE pseudobarcode(
  code VARCHAR (8) NOT NULL
);

INSERT INTO pseudobarcode VALUES ('abcdefgh');
INSERT INTO pseudobarcode VALUES ('bbcdefgh');
INSERT INTO pseudobarcode VALUES ('cbcdefgh');
INSERT INTO pseudobarcode VALUES ('dbcdefgh');

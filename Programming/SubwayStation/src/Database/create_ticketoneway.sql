DROP TABLE IF EXISTS  ticketoneway;
CREATE TABLE ticketoneway (
  id VARCHAR(14) NOT NULL ,
  ticketcode VARCHAR(16) NOT NULL ,
  status INTEGER  NOT NULL ,
  type INTEGER NOT NULL ,
  startStationId INTEGER NOT NULL ,
  endStationId INTEGER NOT NULL ,
  transactionId INTEGER ,
  PRIMARY KEY (id,transactionId),
  FOREIGN KEY (transactionId) REFERENCES station(id)
);

INSERT INTO ticketoneway VALUES ('OW201917110001','e8dc4081b13434b4','1','1','3','7','1');
INSERT INTO ticketoneway VALUES ('OW201916110002','2c01b64843eab357','4','1','2','5','2');

select * from ticketoneway where ticketoneway.ticketcode = 'e8dc4081b13434b4';
DROP TABLE IF EXISTS  ticket24h;
CREATE TABLE ticket24h (
  id VARCHAR(14) NOT NULL ,
  ticketcode VARCHAR(16) NOT NULL ,
  status INTEGER NOT NULL ,
  type INTEGER  NOT NULL ,
  fee FLOAT,
  startTime DATE,
  PRIMARY KEY (id)
);
package Database;

import java.sql.*;

public class ConnectDatabase {
    /**
     * @return return the connect with specified url, username and password
     * @throws ClassNotFoundException throw exception when
     * @throws SQLException
     */
//
    // connect to postgresql
    public Connection Connect() throws ClassNotFoundException, SQLException {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        String user = "postgres";
        String password = "123";
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(url,user,password);
    }

    public Connection Connect(String url, String user, String password) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(url,user,password);
    }
}

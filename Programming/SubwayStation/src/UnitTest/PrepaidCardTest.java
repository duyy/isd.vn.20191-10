package UnitTest;

import Model.PrepaidCard;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PrepaidCardTest {
    private PrepaidCard testCard = new PrepaidCard();
    @Before
    public void setUp()throws NullPointerException{
        testCard.setBalance(10);
    }
    @Test
    public void validPayment() throws NullPointerException{
        boolean[] expected = {true, false};
        boolean[] actual = {
                testCard.validPayment(10),
                testCard.validPayment(9)
        };
        assertArrayEquals(expected,actual);
    }
}
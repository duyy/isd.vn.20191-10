package Model;

public class FareCalculatedFormula {

    //for prepaidcard and one way ticket
    public final float Formula1(float location1, float location2){
        float distance = Math.abs(location1-location2);
        if (distance<=0){
            return 0;
        }else if (distance<=5){
            return (float) 1.9;
        }else {
            float additionalDistance = distance-5;
            return (float) (1.9 + 0.4*additionalDistance/2);
        }
    }

    // is created for any change about formula in the future

}

package Model;

import Model.Ticket;
public class TicketOneWay extends Ticket{

    private int transactionId ;

    private int startStationId;
    private int endStationId;

    public TicketOneWay(String id, int status, int type) {
        super(id, status, type);
    }
    public TicketOneWay(String id, int status, int type,int startStationId,int endStationId, int transactionId){
        super(id, status, type);
        this.startStationId = startStationId;
        this.endStationId = endStationId;
        this.transactionId =transactionId;
    }

    public TicketOneWay() {

    }

    public void setTicketOneWayInfo(int id, int status, int type, int transactionId){
        this.transactionId = transactionId;
    }

    public int getStartStationId(){
        return startStationId;
    }
    public int getEndStationId(){
        return endStationId;
    }
    public int getTransactionId(){
        return transactionId;
    }

    public void setId(String id){
        super.setId(id);
    }
    @Override
    public int getType() {
        return super.getType();
    }

    @Override
    public int getStatus() {
        return super.getStatus();
    }

    @Override
    public String getId() {
        return super.getId();
    }


    public TicketOneWay getTicketOneWayInfo(){
        return this;
    }

    public void setTransactionId(int transactionId){
        this.transactionId =transactionId;
    }

}

package Model;
import Config.Constant;


public class Ticket24h extends Ticket{

    private float price = Constant.baseFare;
    private int startTime ;
    public Ticket24h(String id, int status, int type) {
        super(id, status, type);
    }
    public Ticket24h(String id, int status, int type, float price, int startTime){
        super(id, status, type);
        this.price = price;
        this.startTime = startTime;
    }
}

package Model;

public class Ticket {
    private String id;
    private int status;
    private int type;

    Ticket(String id, int status, int type){
        this.id = id;
        this.status = status;
        this.type = type;
    }

    public Ticket() {
    }

    public String getId(){
        return id;
    }
    public void setId(String id){
        this.id = id;
    }

    public int getType(){
        return type;
    }

    public void setType(int type){
        this.type = type;
    }

    public int getStatus(){
        return status;
    }

    public void setStatus(int status){
        this.status = status;
    }
}

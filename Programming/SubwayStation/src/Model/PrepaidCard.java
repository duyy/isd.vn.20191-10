package Model;
import Config.Constant;
public class PrepaidCard {

    private String id;
    private int status;
    private int type;
    private float balance;
    public PrepaidCard(){

//public class  PrepaidCard extends Ticket{

//    private float price = Constant.baseFare;
//    private int startTime ;
//    public  PrepaidCard(int id, int status, int type) {
//        super(id, status, type);
//    }
//    public  PrepaidCard(int id, int status, int type, float price, int startTime){
//        super(id, status, type);
//        this.price = price;
//        this.startTime = startTime;
//    }
//>>>>>>> 526b36bf1be98e8c1a99bec139340fe406f93c1f
    }

    public PrepaidCard getPrepaidCardInfo(){
        return this;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public boolean validPayment(float price){
        float remaining = balance-price;
        if (remaining >= 0){
            balance= remaining;
            return true;
        }
        return false;
    }
}

package Model;

import java.util.Date;
public class Transaction {
    private int id;
    private int enteredStationId = 0;
    private int exitedStationId = 0;
    private float enteredTime = 0;
    private float exitedTime = 0;
    private float fee = 0;
    public Transaction(int id, int enteredStationId,int exitedStationId,float enteredTime, float exitedTime, float fee){
        this.id = id;
        this.enteredStationId = enteredStationId;
        this.exitedStationId = exitedStationId;
        this.enteredTime = enteredTime;
        this.exitedTime = exitedTime;
        this.fee = fee;
    }

    public Transaction getTransactionInfo(){
        return this;
    }

}

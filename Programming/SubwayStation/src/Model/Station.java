package Model;

import java.sql.*;

public class Station {
    private String name;
    private int id;
    private double location;

    public Station(int id,String name,float location){
        this.id = id;
        this.name =name;
        this.location =location;
    }

    public Station getStationInfo(){
        return this;
    }
    public int getId(){
        return id;
    }

    public  String getName(){
        return  name;
    }

    public double getLocation() {
        return location;
    }

    public void  connectToDatabase(){
//        Connection connection =  // connect to db
    }

}

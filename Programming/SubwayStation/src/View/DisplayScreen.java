package View;

import Config.Constant;
import Controller.*;
import Model.Station;
import Model.Ticket24h;
import Model.TicketOneWay;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.gate.Gate;
import hust.soict.se.scanner.*;
import hust.soict.se.recognizer.*;

import java.sql.SQLException;
import java.util.List;

import java.util.Scanner;

public class DisplayScreen {

    private List<Station> listStation;
    private List<TicketOneWay> listTicketOneWay;
    private List<Ticket24h> listTicket24h;
    private Gate gate = Gate.getInstance();
    private TicketRecognizer recognizer = TicketRecognizer.getInstance();
    private TicketOneWayController ticketOneWayController = TicketOneWayController.getInstance();


    private String[] pseudoBarCodeList = {"abcdefgh","ABCDEFGH","ijklmnop"};

    public DisplayScreen() throws SQLException, ClassNotFoundException{
    }

    public void displayMenu() throws SQLException, ClassNotFoundException, InvalidIDException {
        listStation = new StationController().getStationList();
        listTicketOneWay = new TicketOneWayController().getTicketoneWayList();

        String action = "";
        int actionChoice = 0;
        int stationChoice = 0;

        while (true) {
            Scanner scanner = new Scanner(System.in);

            displayStartScreen();
            do {
                System.out.print("Your choice: ");
                if (scanner.hasNextLine()) action = scanner.nextLine();
                boolean validRequirement = checkValidAction(action);
                if (!validRequirement) {
                    System.out.println("Wrong format. Please enter as the example above!");
                } else {
                    actionChoice  = Integer.parseInt(String.valueOf(action.charAt(0)));
                    stationChoice = Integer.parseInt(String.valueOf(action.charAt(2)));
                }
            } while (!checkValidAction(action));
            String pseudoBarCode;
            int checkpseudoBarCode;
            switch (actionChoice) {
                case 1: //enter station
                    System.out.println("Entering station " + listStation.get(stationChoice-1).getName());
                    pseudoBarCode =  displayAvailableTicketCard();
                    checkpseudoBarCode = checkTypeOfPseudoBarCode(pseudoBarCode);
                    switch (checkpseudoBarCode){
                        case 1: // handle one way ticket
                            ticketOneWayController.TicketOneWayEnterStation(pseudoBarCode,stationChoice);
                            break;
                        case 2: // handle 24h ticket
                            break;
                        case 3: // handle prepaid card
                            break;

                    }
                    pressAnyKeyToContinue();
                    break;

                case 2: //exit station
                    System.out.println("Exiting station " + listStation.get(stationChoice).getName());
                    pseudoBarCode =  displayAvailableTicketCard();
                    checkpseudoBarCode = checkTypeOfPseudoBarCode(pseudoBarCode);
                    switch (checkpseudoBarCode){
                        case 1: // handle one way ticket
                            ticketOneWayController.TicketOneWayExitStation(pseudoBarCode,stationChoice);
                            break;
                        case 2: // handle 24h ticket
                            break;
                        case 3: // handle prepaid card
                            break;

                    }
                    pressAnyKeyToContinue();
                    gate.close();
                    break;
            }
//            scanner.close();
        }
    }

    private String displayAvailableTicketCard() throws InvalidIDException, SQLException, ClassNotFoundException {
        String choice = "";
        Scanner scanner = new Scanner(System.in);
        System.out.println("List of available ticket and card: ");
        printShortenOneWayTicketInfo();
        printShorten24hTicketInfo();
        printShortenPrepaidCardInfo();
        System.out.println();
        do {
            System.out.println("Please provide an id of the ticket or card you want to use:");
            if (scanner.hasNextLine()) choice = scanner.nextLine();
        }while (!checkValidTicketChoice(choice,listTicketOneWay));
        return  choice;
    }




    private void printShortenOneWayTicketInfo() throws InvalidIDException, SQLException, ClassNotFoundException {
        TicketOneWay ticket = ticketOneWayController.getTicketFromCode(recognizer.process("abcdefgh"));
        System.out.print("abcdefgh : One-way ticket between " + listStation.get(ticket.getStartStationId()).getName() + " and " + listStation.get(ticket.getEndStationId()).getName() + ": ");
        switch (ticket.getStatus()){
            case Constant.statusUnsed:
                System.out.print("New -");
                break;
            case Constant.statusNotExist:
                System.out.print("Deleted from database -");
                break;
            case Constant.statusExpired:
                System.out.print("Expired -");
                break;
            case Constant.statusUsing:
                System.out.print("Using -");
                break;
        }
        System.out.println(ticketOneWayController.getFeeOfTicket(ticket.getTransactionId()));

    }
    private void printShortenPrepaidCardInfo() { // not dogne
        System.out.println("ijklmnop: 24h ticket: ");
    }

    private void printShorten24hTicketInfo() { // not done
        System.out.println("ABCDEFGH: prepaid card :");
    }

    private boolean checkValidTicketChoice(String choice, List<TicketOneWay> listTicketOneWay) {
        boolean valid = false;
        for (String code: pseudoBarCodeList) {
            if (choice.equals(code)){
                valid = true;
                return valid;
            }
        }
        return valid;
    }


    private int checkTypeOfPseudoBarCode(String pseudoBarCode) {

        switch (pseudoBarCode) {
            case "abcdefgh":  // oneway ticket
                return 1;
            case "ijklmnop":
            case "ponmlkji": //24h ticket
                return 2;
            case "ABCDEFGH":  // prepaid card
                return 3;
        }
        return -1;
    }

    private int displayMethodScreen() {
        int choice;
        String[] choiceName = {"One way ticket", "24h ticket", "Prepaid card", "Back"};
        System.out.println("Choose type of ticket/card you want to use: ");
        for (int i = 1;i<=choiceName.length;i++){
            System.out.println(i+ "." + choiceName[i-1]);
        }
        Scanner scanner = new Scanner(System.in);
        do {
            choice = scanner.nextInt();
        }while ((choice <=0) || (choice > choiceName.length));
//        scanner.close();
        return choice = 1;
    }

    private void displayStartScreen() {
        System.out.println("\n\nAvailable actions : 1 - Enter Station, 2 - Exit Station\n" +
                "You can provide  a combination of 2 numbers (1 or 2) to enter or exit a sation (using hypen in between). For instance, the combination \"2-4\" will bring you to exit the station Chaletet ");
                //list of station
        for (Station station: listStation ) {
            System.out.println(station.getId() + "." + station.getName());
        }
    }

    private void pressAnyKeyToContinue(){
        System.out.println("Press any key to continue...");
        Scanner scanner = new Scanner(System.in);
        char c = scanner.next().charAt(0);
//        scanner.close();
    }

    private boolean checkValidAction(String action) {
        if(action.length() != 3 ){
            return false;
        }

        char inout = action.charAt(0);
        char stationSerial = action.charAt(2);
        if (inout !='1' && inout != '2'){
            return false;
        }
        if (action.charAt(1) != '-'){
            return false;
        }
        if (stationSerial < '1' || stationSerial >'9'){
            return false;
        }
        return true;
    }

}
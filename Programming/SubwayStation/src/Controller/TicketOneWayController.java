package Controller;

import Config.Constant;
import Database.ConnectDatabase;

import Model.FareCalculatedFormula;
import Model.Ticket;
import Model.TicketOneWay;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.gate.Gate;
import hust.soict.se.recognizer.TicketRecognizer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TicketOneWayController extends TicketController {
    private static List<TicketOneWay> ticketOneWayList = new ArrayList<>();
    private TicketRecognizer recognizer = TicketRecognizer.getInstance();
    private Gate gate =Gate.getInstance();
    private static final TicketOneWayController instance = new TicketOneWayController();
    public static TicketOneWayController getInstance() throws SQLException, ClassNotFoundException {
        Connection connection = new  ConnectDatabase().Connect();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM ticketoneway";
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()){
            TicketOneWay tmp = new TicketOneWay(resultSet.getString(1),
                    resultSet.getInt(3),
                    resultSet.getInt(4),
                    resultSet.getInt(5),
                    resultSet.getInt(6),
                    resultSet.getInt(7)
            );
            ticketOneWayList.add(tmp);
        }
        connection.close();
        return instance;
    }

    public List<TicketOneWay> getTicketoneWayList(){
        return ticketOneWayList;
    }

    /**
     * @param pseudoBarCode the pseudoBarCode that user input
     * @param stationChoice the station choice when user choose enter or exit which station
     * @throws InvalidIDException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void TicketOneWayEnterStation(String pseudoBarCode, int stationChoice) throws InvalidIDException, SQLException, ClassNotFoundException {
        String ticketCode = recognizer.process(pseudoBarCode);
        TicketOneWay ticket = getTicketFromCode(ticketCode);
        if (ticket.getStatus() != 1){
            switch (ticket.getStatus()){
                case 2:
                    System.out.println("Ticket doesnt exist");
                    break;
                case 3:
                    System.out.println("Ticket has been used. Status is expired");
                    break;
                case 4:
                    System.out.println("Ticket is using to enter station before");
                    break;
                default:
                    System.out.println("Error ticket");
                    break;
            }
        }else if (checkTicketValidation(ticket,stationChoice)){
            updateTicket(ticket,stationChoice, Constant.statusUsing);// update transaction
            gate.open();
        }//pass check validable sucessfully
        displayBasicInfoTicket(ticket);
    }

    /**
     * @param ticket get ticket as parameter to update attributes into database
     * @param stationChoice the station choice when user choose enter or exit which station
     * @param newStatus new status that apply to ticket
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void updateTicket(TicketOneWay ticket, int stationChoice,int newStatus) throws SQLException, ClassNotFoundException {
        createNewTransaction(ticket,stationChoice);
        updateTicketStatus(ticket,newStatus);
    }

    /**
     * @param ticket get ticket as parameter to update attributes into database
     * @param newStatus new status that apply to ticket
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void updateTicketStatus(TicketOneWay ticket, int newStatus) throws SQLException, ClassNotFoundException {
        Connection connection = new ConnectDatabase().Connect();
        Statement statement = connection.createStatement();
        String sql = String.format("UPDATE ticketoneway SET status ='%d' WHERE id = '%s'",newStatus,ticket.getId() ) ;
        statement.executeUpdate(sql);
//        if (resultSet.next());
    }

    /**
     * @param ticket display all needed information of ticket
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void displayBasicInfoTicket(TicketOneWay ticket) throws SQLException, ClassNotFoundException {
        System.out.print("Type: One way ticket");
        System.out.println("\tId: " + ticket.getId());
        float fee = getFeeOfTicket(ticket.getTransactionId());
        System.out.println("Balance: " + fee);
    }

    /**
     * @param transactionId return the fee of a ticket by a transaction id
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public float getFeeOfTicket(int transactionId) throws SQLException, ClassNotFoundException {
        Connection connection = new  ConnectDatabase().Connect();
        Statement statement = connection.createStatement();
        String sql = String.format("SELECT * FROM transactions WHERE transactions.id ='%s'",transactionId );
        ResultSet resultSet = statement.executeQuery(sql);
        if (resultSet.next());
        float fee = resultSet.getFloat(6);
        connection.close();
        return fee;
    }

    /**
     * @param ticket ticket that need to check with station choice
     * @param stationChoice the station choice when user choose enter or exit which station
     * @return return 1 if the station choice is between 2 station of ticket, return 0 otherwise
     */
    private int checkValidStation(TicketOneWay ticket, int stationChoice) {
//        System.out.println("sation choice = " + stationChoice + " startid /endid "+ ticket.getStartStationId() + "  "+ ticket.getEndStationId());
        if (stationChoice<ticket.getStartStationId() || stationChoice> ticket.getEndStationId()) return 1;
        int enterStation;
//        if (stationChoice)
        return 0;
    }

    /**
     * @param ticketCode  ticketcode that generated from ticket regconizer
     * @return return a instance of ticket one way
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public TicketOneWay getTicketFromCode(String ticketCode) throws SQLException, ClassNotFoundException {
        Connection connection = new  ConnectDatabase().Connect();
        Statement statement = connection.createStatement();
        String sql = String.format("SELECT * FROM ticketoneway WHERE ticketoneway.ticketcode ='%s'", ticketCode);

        ResultSet resultSet = statement.executeQuery(sql);
        if (resultSet.next());
        TicketOneWay ticket = new TicketOneWay(resultSet.getString(1),
                resultSet.getInt(3),
                resultSet.getInt(4),
                resultSet.getInt(5),
                resultSet.getInt(6),
                resultSet.getInt(7));
        connection.close();
        return ticket;
    }


    /**
     * @param ticket update the ticket's entered station id with station id
     * @param stationId station id that update to ticket
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void createNewTransaction(TicketOneWay ticket,int stationId) throws SQLException, ClassNotFoundException {
        Connection connection = new ConnectDatabase().Connect();
        Statement statement = connection.createStatement();

        String sql= String.format("UPDATE transactions SET enteredStationId ='%s' WHERE id = '%s'",stationId,ticket.getTransactionId() ) ;
        statement.executeUpdate(sql);
//        if (resultSet.next());
    }

    /**
     * @param ticket check ticket validation to enter or exit station
     * @param stationChoice with station choice to check
     * @return return true if ticket is valid to enter/exit the station choice, otherwise return false
     */
    private boolean checkTicketValidation(TicketOneWay ticket, int stationChoice){
        if (ticket.getId()==null){
            System.out.println("Ticket doesnt exist.");
            return false;
        }else if (checkValidStation(ticket,stationChoice) ==1){ // ga nam ngoai pham vi cho phep
            System.out.println("The entering station is not valid.");
            return false;
        }
        return true;
    }
    /**
     * @param pseudoBarCode the pseudoBarCode that user input
     * @param stationChoice the station choice when user choose enter or exit which station
     * @throws InvalidIDException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void TicketOneWayExitStation(String pseudoBarCode, int stationChoice) throws InvalidIDException, SQLException, ClassNotFoundException {
        String ticketCode = recognizer.process(pseudoBarCode);
        TicketOneWay ticket = getTicketFromCode(ticketCode);
        if (ticket.getStatus() != Constant.statusUsing){
            switch (ticket.getStatus()){
                case Constant.statusNotExist:
                    System.out.println("Ticket doesnt exist");
                    break;
                case Constant.statusUnsed:
                    System.out.println("Cant use unsed ticket to exit station");
                    break;
                case Constant.statusExpired:
                    System.out.println("Ticket has been used. Status is expired");
                    break;
                default:
                    System.out.println("Error ticket");
                    break;
            }
        }else if (checkTicketValidation(ticket,stationChoice)){
            updateTicket(ticket,stationChoice, Constant.statusExpired);// update transaction
            updateBalance(ticket,stationChoice);
            gate.open();
        }//pass check validable sucessfully

        displayBasicInfoTicket(ticket);
    }

    /**
     * @param ticket update balance of ticket
     * @param stationChoice with station choice as entered station
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void updateBalance(TicketOneWay ticket, int stationChoice) throws SQLException, ClassNotFoundException {
        Connection connection = new  ConnectDatabase().Connect();
        Statement statement = connection.createStatement();
        String sql = String.format("SELECT enteredstationid FROM transactions WHERE id ='%s'",ticket.getTransactionId() );
        ResultSet resultSet = statement.executeQuery(sql);
        int id= 9;
        while ((resultSet.next())){
            id = Integer.parseInt(resultSet.getString(1)) ;
        }
        float enter = new StationController().getStationLocation(id);
        float exit = new StationController().getStationLocation(stationChoice);
        float remain = new FareCalculatedFormula().Formula1(enter,exit);
        String sql1 = String.format("UPDATE transactions SET fee ='%f' WHERE id = '%s'",remain,ticket.getTransactionId() ) ;
        statement.executeUpdate(sql1);

    }
}

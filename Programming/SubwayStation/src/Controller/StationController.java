package Controller;

import Database.ConnectDatabase;
import Model.Station;
import Model.TicketOneWay;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StationController {
    private List<Station> stationList = new ArrayList<Station>();

    public StationController() throws SQLException, ClassNotFoundException {
        Connection connection = new ConnectDatabase().Connect();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM station";
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()){
            Station tmp = new Station(resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getFloat(3));
            stationList.add(tmp);
        }
        connection.close();
    }
    public List<Station> getStationList (){
        return stationList;
    }

    public float getStationLocation(int stationId){
        for (Station station: stationList) {
            if ( station.getId() == stationId){
                return (float) station.getLocation();
            }
        }
        return 0;
    }
}

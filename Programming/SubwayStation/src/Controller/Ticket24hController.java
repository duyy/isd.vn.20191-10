package Controller;

import Database.ConnectDatabase;
import Model.Ticket24h;
import Model.TicketOneWay;
import hust.soict.se.gate.Gate;
import hust.soict.se.recognizer.TicketRecognizer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
//
public class Ticket24hController extends TicketController{
    private static List<Ticket24h> ticket24hList = new ArrayList<>();
    private TicketRecognizer recognizer = TicketRecognizer.getInstance();
    private Gate gate =Gate.getInstance();
    private static final TicketOneWayController instance = new TicketOneWayController();
    public static TicketOneWayController getInstance() throws SQLException, ClassNotFoundException {
        Connection connection = new  ConnectDatabase().Connect();
        Statement statement = connection.createStatement();
        String sql = "SELECT * FROM ticket24h";
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()){
            Ticket24h tmp = new Ticket24h(resultSet.getString(1),
                    resultSet.getInt(2),
                    resultSet.getInt(3),
                    resultSet.getFloat(4),
                    resultSet.getInt(5)
            );
            ticket24hList.add(tmp);
        }
        connection.close();
        return instance;
    }

    public List<Ticket24h> getTicketoneWayList(){
        return ticket24hList;
    }
    public List<Ticket24h> getTicket24hList(){
        return ticket24hList;
    }
}

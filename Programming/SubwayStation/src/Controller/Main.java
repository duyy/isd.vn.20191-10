package Controller;

import View.DisplayScreen;
import hust.soict.se.customexception.InvalidIDException;
import  hust.soict.se.scanner.CardScanner;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws InvalidIDException, SQLException, ClassNotFoundException {

        DisplayScreen screen = new DisplayScreen();
        screen.displayMenu();

    }

}

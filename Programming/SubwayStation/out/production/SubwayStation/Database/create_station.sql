DROP TABLE IF EXISTS  station;
CREATE TABLE station (
  id INTEGER NOT NULL ,
  name VARCHAR (255) NOT NULL,
  location FLOAT NOT NULL,
  PRIMARY KEY (id)
);
INSERT INTO station VALUES ('1','Saint Lazare','0');
INSERT INTO station VALUES (2,'Madeleine',5);
INSERT INTO station VALUES (3,'Pyramides',  8.5);
INSERT INTO station VALUES (4,'Chatelet',  11.3);
INSERT INTO station VALUES (5,'Gare de Lyon',  15.8);
INSERT INTO station VALUES (6,'Bercy',  18.9);
INSERT INTO station VALUES (7,'Cour Saint-Emilion',22);
INSERT INTO station VALUES (8,'Bibliotheque Francois Mitterrand',  25.3);
INSERT INTO station VALUES (9,'Olympiades',  28.8);

select *
from station;




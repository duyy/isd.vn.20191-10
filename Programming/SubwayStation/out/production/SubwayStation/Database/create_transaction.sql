DROP TABLE IF EXISTS  transactions;
CREATE TABLE transactions (
  id INTEGER NOT NULL,
  enteredStationId INTEGER  ,
  exitedStationId INTEGER  ,
  enteredTime VARCHAR(255) ,
  exitedTime VARCHAR(255) ,
  fee FLOAT ,
  PRIMARY KEY (id),
  FOREIGN KEY (enteredStationId) REFERENCES station(id),
  FOREIGN KEY (exitedStationId) REFERENCES station(id)
);

INSERT INTO transactions VALUES ('1','2','7','Wed Dec 05 22:22:22 ICT 2019','Wed Dec 05 23:23:23 ICT 2019','1.9');
INSERT INTO transactions VALUES ('2','2','5','Thu Dec 05 10:10:10 ICT 2019','Thu Dec 05 11:11:11 ICT 2019','6.16');

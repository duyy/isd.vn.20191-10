## Work assignment in Architecture Design 
### Detail Design and Data Modeling

#### Leader : Nguyễn Duy Ý
### Nguyễn Duy Ý
1. In the SDD, design database for prepaidcard
2. In the SDD, design database for station
3. In the SDD, design database for ticket


### Bùi Ngọc Tú
1. Design Conceptual Data Model Diagram
2. In the SDD, design database for Oneway ticket
3. In the SDD, design database for 24h ticket

### Lê Thanh Tùng
1. In the SDD, design Logical Data Model
